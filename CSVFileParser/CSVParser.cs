﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVFileParser
{
    public class CSVParser
    {
        private string _CSVFilePath;
        public CSVParser(string filePath)
        {
            _CSVFilePath = filePath;
        }

        public bool ParseFile(ref IList<StoreDataModel> storeDataList)
        {
            bool isFileParsedSuccessfully = false;
            try
            {
                string fileName = Path.GetFileNameWithoutExtension(_CSVFilePath);
                string[] fileNameParts = fileName.Split('_');
                StreamReader stream = new StreamReader(_CSVFilePath);
                var csv = new CsvReader(stream);
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.SkipEmptyRecords = true;
                while (csv.Read())
                {
                    StoreDataModel storeData = new StoreDataModel();
                    storeData.StoreCode = fileNameParts[0];
                    storeData.PurchaseOrderWeekNumber = Convert.ToInt32(fileNameParts[1]);
                    storeData.PurchaseOrderYear = fileNameParts[2];
                    storeData.SupplierName = csv[0];
                    storeData.SupplierType = csv[1];
                    storeData.CostOfOrder = Convert.ToDecimal(csv[2]);

                    storeDataList.Add(storeData);
                }

                isFileParsedSuccessfully = true;
                csv.Dispose();
                stream.Close();
            }
            catch(Exception ex)
            {
                isFileParsedSuccessfully = false;
            }

            return isFileParsedSuccessfully;
        }
    }
}
