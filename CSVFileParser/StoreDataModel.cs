﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVFileParser
{
    public class StoreDataModel
    {
        public string StoreCode { set; get; }
        public int PurchaseOrderWeekNumber { set; get; }
        public string PurchaseOrderYear { set; get; }
        public string SupplierName { set; get; }
        public string SupplierType { set; get; }
        public decimal CostOfOrder { set; get; }
    }
}
