﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using CSVFileParser;
using System.Linq.Expressions;

namespace SpurOrderingSystem_ReportingTool
{
    public partial class Dashboard : Form
    {
        int fileProcessingDuration = 0;

        IList<StoreDataModel> storeData = new List<StoreDataModel>();
        IList<string> storeCodesList;
        IList<string> suppliersList;
        IList<string> supplierTypesList;
        IList<string> weekNumbersList = new List<string>();
        IList<string> yearsList;

        string m_AllStoresDefaultItem = "All Stores";
        string m_AllSupplierTypesDefaultItem = "All Supplier Types";
        string m_AllSupplierDefaultItem = "All Suppliers";
        string m_AllWeekNumberDefaultItem = "All Weeks";
        string m_AllYearDefaultItem = "All Years";
        public Dashboard()
        {
            InitializeComponent();
        }

        private void ChooseCSVFilesDirectoryButton_Click(object sender, EventArgs e)
        {
            DialogResult folderBrowserResult = CSVFolderBrowserDialog.ShowDialog();
            if (folderBrowserResult == DialogResult.OK)
            {
                CSVFilesSelectedDirectoryTextBox.Text = CSVFolderBrowserDialog.SelectedPath;
                StartFilesProcessing.Enabled = true;
                LoadLookupData.Enabled = false;
                FinancialReportingInputsGroupbox.Enabled = false;
                CostOfOrders.Text = string.Empty;
                ReportOutputListbox.Enabled = false;
            }
        }

        private void LoadLookupData_Click(object sender, EventArgs e)
        {
            FinancialReportingInputsGroupbox.Enabled = false;
            CostOfOrders.Text = string.Empty;
            ReportOutputListbox.Enabled = false;
            Task fileProcessingTask = Task.Factory.StartNew(() =>
                {
                    storeCodesList = storeData.Select(item => item.StoreCode).Distinct().ToList();
                    supplierTypesList = storeData.Select(item => item.SupplierType).Distinct().ToList();
                    suppliersList = storeData.Select(item => item.SupplierName).Distinct().ToList();
                    //weekNumbersList = storeData.Select(item => item.PurchaseOrderWeekNumber).Distinct().ToList();
                    yearsList = storeData.Select(item => item.PurchaseOrderYear).Distinct().ToList();

                    storeCodesList.Insert(0, m_AllStoresDefaultItem);
                    supplierTypesList.Insert(0, m_AllSupplierTypesDefaultItem);
                    suppliersList.Insert(0, m_AllSupplierDefaultItem);
                    weekNumbersList.Insert(0, m_AllWeekNumberDefaultItem);
                    for (int weekId = 1; weekId < 53; weekId++)
                    {
                        weekNumbersList.Add(weekId.ToString());
                    }

                    yearsList.Insert(0, m_AllYearDefaultItem);
                }
                )
                .ContinueWith(
                (processingTask) =>
                {
                    StartFilesProcessing.Enabled = true;
                    FinancialReportingInputsGroupbox.Enabled = true;
                    ReportOutputListbox.Enabled = true;
                    ChooseStoresComboBox.DataSource = storeCodesList;
                    ChooseSupplierTypeComboBox.DataSource = supplierTypesList;
                    ChooseSupplierComboBox.DataSource = suppliersList;
                    ChooseWeekNumberCombobox.DataSource = weekNumbersList;
                    ChooseWeekNumberCombobox.SelectedIndex = 0;
                    ChooseYearCombobox.DataSource = yearsList;
                }, TaskScheduler.FromCurrentSynchronizationContext()
                );
        }

        private void ProcessFiles(string selectedDirectoryPath)
        {
            int startTime = Environment.TickCount;
            string[] directoryFiles = Directory.GetFiles(selectedDirectoryPath, "*.csv", SearchOption.AllDirectories);
            foreach (string file in directoryFiles)
            {
                var fileProcessor = new CSVParser(file);
                bool isfileParsedSuccessfully = fileProcessor.ParseFile(ref storeData);
                if (isfileParsedSuccessfully)
                {

                }
            }

            int endTime = Environment.TickCount;

            fileProcessingDuration = (endTime - startTime) / 1000;
        }

        private void CalculateCost_Click(object sender, EventArgs e)
        {
            string chosenStore = ChooseStoresComboBox.SelectedValue.ToString();
            string chosenSupplierType = ChooseSupplierTypeComboBox.SelectedValue.ToString();
            string chosenSupplier = ChooseSupplierComboBox.SelectedValue.ToString();
            string chosenWeekNumber = ChooseWeekNumberCombobox.SelectedValue.ToString();
            string chosenYear = ChooseYearCombobox.SelectedValue.ToString();
            decimal totalCost = 0.0m;
            double executionTime = 0.0;

            Task calculationTask = Task.Factory.StartNew(() =>
            {
                int start = Environment.TickCount;
                IList<StoreDataModel> filteredResults = null;

                if (!chosenWeekNumber.Equals(m_AllWeekNumberDefaultItem))
                {
                    if (filteredResults != null)
                    {
                        filteredResults = filteredResults.Where(item => item.PurchaseOrderWeekNumber == Convert.ToInt32(chosenWeekNumber)).ToList();
                    }
                    else
                    {
                        filteredResults = storeData.Where(item => item.PurchaseOrderWeekNumber == Convert.ToInt32(chosenWeekNumber)).ToList();
                    }
                }

                if (!chosenYear.Equals(m_AllYearDefaultItem))
                {
                    if (filteredResults != null)
                    {
                        filteredResults = filteredResults.Where(item => item.PurchaseOrderYear == chosenYear).ToList();
                    }
                    else
                    {
                        filteredResults = storeData.Where(item => item.PurchaseOrderYear == chosenYear).ToList();
                    }
                }

                if (!chosenSupplierType.Equals(m_AllSupplierTypesDefaultItem))
                {
                    if (filteredResults != null)
                    {
                        filteredResults = filteredResults.Where(item => item.SupplierType == chosenSupplierType).ToList();
                    }
                    else
                    {
                        filteredResults = storeData.Where(item => item.SupplierType == chosenSupplierType).ToList();
                    }
                }

                if (!chosenStore.Equals(m_AllStoresDefaultItem))
                {
                    if (filteredResults != null)
                    {
                        filteredResults = filteredResults.Where(item => item.StoreCode == chosenStore).ToList();
                    }
                    else
                    {
                        filteredResults = storeData.Where(item => item.StoreCode == chosenStore).ToList();
                    }
                }

                if (!chosenSupplier.Equals(m_AllSupplierDefaultItem))
                {
                    if (filteredResults != null)
                    {
                        filteredResults = filteredResults.Where(item => item.SupplierName == chosenSupplier).ToList();
                    }
                    else
                    {
                        filteredResults = storeData.Where(item => item.SupplierName == chosenSupplier).ToList();
                    }
                }

                if (filteredResults != null)
                {
                    totalCost = filteredResults.Sum(item => item.CostOfOrder);
                }
                else
                {
                    totalCost = storeData.Sum(item => item.CostOfOrder);
                }

                int end = Environment.TickCount;

                executionTime = (end - start) / 1000;
            })
            .ContinueWith(
            (outputTask) =>
            {
                ReportOutputListbox.Visible = true;
                string[] reportInputs = new[] {
                    string.Format("Financial Report Input Parameters (Execution Time: {0}s)", executionTime),
                    string.Format("Store: {0}", chosenStore),
                    string.Format("Supplier Type: {0}", chosenSupplierType),
                    string.Format("Supplier Name: {0}", chosenSupplier),
                    string.Format("Week Number: {0}", chosenWeekNumber),
                    string.Format("Year: {0}", chosenYear),
                    string.Format("Total Cost: {0:C}", totalCost),
                    string.Format("=========================================================================="),
            };
                CostOfOrders.Text = string.Format("{0:C}", totalCost);
                foreach (var item in reportInputs)
                {
                    ReportOutputListbox.Items.Add(item);
                }
            }, TaskScheduler.FromCurrentSynchronizationContext()
            );
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            ProgressIndicator.Visible = false;
            LoadLookupData.Enabled = false;
            FinancialReportingInputsGroupbox.Enabled = false;
            
        }

        private void StartFilesProcessing_Click(object sender, EventArgs e)
        {
            string selectedDirectoryPath = CSVFilesSelectedDirectoryTextBox.Text.Trim();
            if (!string.IsNullOrEmpty(selectedDirectoryPath))
            {
                if (Directory.Exists(selectedDirectoryPath))
                {
                    storeData.Clear();
                    ChooseCSVFilesDirectoryButton.Enabled = false;
                    StartFilesProcessing.Enabled = false;
                    LoadLookupData.Enabled = false;
                    FinancialReportingInputsGroupbox.Enabled = false;
                    CostOfOrders.Text = string.Empty;
                    ReportOutputListbox.Enabled = false;
                    ProgressIndicator.Visible = true;
                    Task fileProcessingTask = Task.Factory.StartNew(() => ProcessFiles(selectedDirectoryPath))
                        .ContinueWith(
                        (antecedent) =>
                        {
                            ChooseCSVFilesDirectoryButton.Enabled = true;
                            ProgressIndicator.Visible = false;
                            LoadLookupData.Enabled = true;
                            //StartFilesProcessing.Enabled = true;
                            FileProcessingDuration.Text = string.Format("CSV files processing took {0} seconds.", fileProcessingDuration);
                        }, TaskScheduler.FromCurrentSynchronizationContext()
                        );
                }
                else
                {
                    MessageBox.Show(this, "The selected directory does not exist, please select a valid directory", "Reporting Dashboard", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                }
            }
            else
            {
                MessageBox.Show(this, "Please select csv files directory to see the list of stores", "Reporting Dashboard", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }
        }
    }
}
