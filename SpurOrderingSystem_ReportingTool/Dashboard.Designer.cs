﻿namespace SpurOrderingSystem_ReportingTool
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.CSVFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.CSVFilesSelectedDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.ChooseCSVFilesDirectoryButton = new System.Windows.Forms.Button();
            this.ChooseCSVFilesDirectoryGroupBox = new System.Windows.Forms.GroupBox();
            this.ProgressIndicator = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LoadLookupData = new System.Windows.Forms.Button();
            this.FinancialReportingInputsGroupbox = new System.Windows.Forms.GroupBox();
            this.ChooseYearCombobox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ChooseWeekNumberCombobox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CalculateCost = new System.Windows.Forms.Button();
            this.ChooseSupplierComboBox = new System.Windows.Forms.ComboBox();
            this.ChooseSupplierLabel = new System.Windows.Forms.Label();
            this.ChooseSupplierTypeComboBox = new System.Windows.Forms.ComboBox();
            this.ChooseSupplierTypeLabel = new System.Windows.Forms.Label();
            this.ChooseStoresComboBox = new System.Windows.Forms.ComboBox();
            this.ChooseStoreLabel = new System.Windows.Forms.Label();
            this.CostOfOrdersLabel = new System.Windows.Forms.Label();
            this.CostOfOrders = new System.Windows.Forms.Label();
            this.ReportOutputListbox = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.StartFilesProcessing = new System.Windows.Forms.Button();
            this.FileProcessingDuration = new System.Windows.Forms.Label();
            this.ChooseCSVFilesDirectoryGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressIndicator)).BeginInit();
            this.FinancialReportingInputsGroupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // CSVFolderBrowserDialog
            // 
            this.CSVFolderBrowserDialog.ShowNewFolderButton = false;
            // 
            // CSVFilesSelectedDirectoryTextBox
            // 
            this.CSVFilesSelectedDirectoryTextBox.Location = new System.Drawing.Point(6, 19);
            this.CSVFilesSelectedDirectoryTextBox.Name = "CSVFilesSelectedDirectoryTextBox";
            this.CSVFilesSelectedDirectoryTextBox.ReadOnly = true;
            this.CSVFilesSelectedDirectoryTextBox.Size = new System.Drawing.Size(870, 20);
            this.CSVFilesSelectedDirectoryTextBox.TabIndex = 2;
            // 
            // ChooseCSVFilesDirectoryButton
            // 
            this.ChooseCSVFilesDirectoryButton.Location = new System.Drawing.Point(6, 45);
            this.ChooseCSVFilesDirectoryButton.Name = "ChooseCSVFilesDirectoryButton";
            this.ChooseCSVFilesDirectoryButton.Size = new System.Drawing.Size(161, 21);
            this.ChooseCSVFilesDirectoryButton.TabIndex = 3;
            this.ChooseCSVFilesDirectoryButton.Text = "Choose CSV File(s) Directory";
            this.ChooseCSVFilesDirectoryButton.UseVisualStyleBackColor = true;
            this.ChooseCSVFilesDirectoryButton.Click += new System.EventHandler(this.ChooseCSVFilesDirectoryButton_Click);
            // 
            // ChooseCSVFilesDirectoryGroupBox
            // 
            this.ChooseCSVFilesDirectoryGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.ChooseCSVFilesDirectoryGroupBox.Controls.Add(this.ProgressIndicator);
            this.ChooseCSVFilesDirectoryGroupBox.Controls.Add(this.CSVFilesSelectedDirectoryTextBox);
            this.ChooseCSVFilesDirectoryGroupBox.Controls.Add(this.ChooseCSVFilesDirectoryButton);
            this.ChooseCSVFilesDirectoryGroupBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ChooseCSVFilesDirectoryGroupBox.Location = new System.Drawing.Point(12, 43);
            this.ChooseCSVFilesDirectoryGroupBox.Name = "ChooseCSVFilesDirectoryGroupBox";
            this.ChooseCSVFilesDirectoryGroupBox.Size = new System.Drawing.Size(887, 104);
            this.ChooseCSVFilesDirectoryGroupBox.TabIndex = 4;
            this.ChooseCSVFilesDirectoryGroupBox.TabStop = false;
            this.ChooseCSVFilesDirectoryGroupBox.Text = "CSV File(s) Directory Selection";
            // 
            // ProgressIndicator
            // 
            this.ProgressIndicator.Image = ((System.Drawing.Image)(resources.GetObject("ProgressIndicator.Image")));
            this.ProgressIndicator.Location = new System.Drawing.Point(192, 45);
            this.ProgressIndicator.Name = "ProgressIndicator";
            this.ProgressIndicator.Size = new System.Drawing.Size(410, 48);
            this.ProgressIndicator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ProgressIndicator.TabIndex = 4;
            this.ProgressIndicator.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(17, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 19);
            this.label1.TabIndex = 5;
            this.label1.Text = "Reporting Tool for Spur Ordering System";
            // 
            // LoadLookupData
            // 
            this.LoadLookupData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadLookupData.Location = new System.Drawing.Point(632, 113);
            this.LoadLookupData.Name = "LoadLookupData";
            this.LoadLookupData.Size = new System.Drawing.Size(256, 23);
            this.LoadLookupData.TabIndex = 6;
            this.LoadLookupData.Text = "Load Stores, Suppliers and Supplier Types Data";
            this.LoadLookupData.UseVisualStyleBackColor = true;
            this.LoadLookupData.Click += new System.EventHandler(this.LoadLookupData_Click);
            // 
            // FinancialReportingInputsGroupbox
            // 
            this.FinancialReportingInputsGroupbox.Controls.Add(this.ChooseYearCombobox);
            this.FinancialReportingInputsGroupbox.Controls.Add(this.label2);
            this.FinancialReportingInputsGroupbox.Controls.Add(this.ChooseWeekNumberCombobox);
            this.FinancialReportingInputsGroupbox.Controls.Add(this.label3);
            this.FinancialReportingInputsGroupbox.Controls.Add(this.CalculateCost);
            this.FinancialReportingInputsGroupbox.Controls.Add(this.ChooseSupplierComboBox);
            this.FinancialReportingInputsGroupbox.Controls.Add(this.ChooseSupplierLabel);
            this.FinancialReportingInputsGroupbox.Controls.Add(this.ChooseSupplierTypeComboBox);
            this.FinancialReportingInputsGroupbox.Controls.Add(this.ChooseSupplierTypeLabel);
            this.FinancialReportingInputsGroupbox.Controls.Add(this.ChooseStoresComboBox);
            this.FinancialReportingInputsGroupbox.Controls.Add(this.ChooseStoreLabel);
            this.FinancialReportingInputsGroupbox.Location = new System.Drawing.Point(13, 153);
            this.FinancialReportingInputsGroupbox.Name = "FinancialReportingInputsGroupbox";
            this.FinancialReportingInputsGroupbox.Size = new System.Drawing.Size(886, 109);
            this.FinancialReportingInputsGroupbox.TabIndex = 7;
            this.FinancialReportingInputsGroupbox.TabStop = false;
            this.FinancialReportingInputsGroupbox.Text = "Financial Reports Input Criteria";
            // 
            // ChooseYearCombobox
            // 
            this.ChooseYearCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChooseYearCombobox.FormattingEnabled = true;
            this.ChooseYearCombobox.Location = new System.Drawing.Point(618, 44);
            this.ChooseYearCombobox.Name = "ChooseYearCombobox";
            this.ChooseYearCombobox.Size = new System.Drawing.Size(257, 21);
            this.ChooseYearCombobox.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(538, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Choose Year: ";
            // 
            // ChooseWeekNumberCombobox
            // 
            this.ChooseWeekNumberCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChooseWeekNumberCombobox.FormattingEnabled = true;
            this.ChooseWeekNumberCombobox.Location = new System.Drawing.Point(618, 17);
            this.ChooseWeekNumberCombobox.Name = "ChooseWeekNumberCombobox";
            this.ChooseWeekNumberCombobox.Size = new System.Drawing.Size(257, 21);
            this.ChooseWeekNumberCombobox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(511, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Choose Week No.: ";
            // 
            // CalculateCost
            // 
            this.CalculateCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalculateCost.ForeColor = System.Drawing.Color.Navy;
            this.CalculateCost.Location = new System.Drawing.Point(763, 71);
            this.CalculateCost.Name = "CalculateCost";
            this.CalculateCost.Size = new System.Drawing.Size(112, 23);
            this.CalculateCost.TabIndex = 6;
            this.CalculateCost.Text = "Calculate Cost";
            this.CalculateCost.UseVisualStyleBackColor = true;
            this.CalculateCost.Click += new System.EventHandler(this.CalculateCost_Click);
            // 
            // ChooseSupplierComboBox
            // 
            this.ChooseSupplierComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChooseSupplierComboBox.FormattingEnabled = true;
            this.ChooseSupplierComboBox.Location = new System.Drawing.Point(131, 71);
            this.ChooseSupplierComboBox.Name = "ChooseSupplierComboBox";
            this.ChooseSupplierComboBox.Size = new System.Drawing.Size(257, 21);
            this.ChooseSupplierComboBox.TabIndex = 5;
            // 
            // ChooseSupplierLabel
            // 
            this.ChooseSupplierLabel.AutoSize = true;
            this.ChooseSupplierLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChooseSupplierLabel.Location = new System.Drawing.Point(35, 74);
            this.ChooseSupplierLabel.Name = "ChooseSupplierLabel";
            this.ChooseSupplierLabel.Size = new System.Drawing.Size(90, 13);
            this.ChooseSupplierLabel.TabIndex = 4;
            this.ChooseSupplierLabel.Text = "Choose Supplier: ";
            // 
            // ChooseSupplierTypeComboBox
            // 
            this.ChooseSupplierTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChooseSupplierTypeComboBox.FormattingEnabled = true;
            this.ChooseSupplierTypeComboBox.Location = new System.Drawing.Point(131, 44);
            this.ChooseSupplierTypeComboBox.Name = "ChooseSupplierTypeComboBox";
            this.ChooseSupplierTypeComboBox.Size = new System.Drawing.Size(257, 21);
            this.ChooseSupplierTypeComboBox.TabIndex = 3;
            // 
            // ChooseSupplierTypeLabel
            // 
            this.ChooseSupplierTypeLabel.AutoSize = true;
            this.ChooseSupplierTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChooseSupplierTypeLabel.Location = new System.Drawing.Point(8, 47);
            this.ChooseSupplierTypeLabel.Name = "ChooseSupplierTypeLabel";
            this.ChooseSupplierTypeLabel.Size = new System.Drawing.Size(117, 13);
            this.ChooseSupplierTypeLabel.TabIndex = 2;
            this.ChooseSupplierTypeLabel.Text = "Choose Supplier Type: ";
            // 
            // ChooseStoresComboBox
            // 
            this.ChooseStoresComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChooseStoresComboBox.FormattingEnabled = true;
            this.ChooseStoresComboBox.Location = new System.Drawing.Point(131, 17);
            this.ChooseStoresComboBox.Name = "ChooseStoresComboBox";
            this.ChooseStoresComboBox.Size = new System.Drawing.Size(257, 21);
            this.ChooseStoresComboBox.TabIndex = 1;
            // 
            // ChooseStoreLabel
            // 
            this.ChooseStoreLabel.AutoSize = true;
            this.ChooseStoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChooseStoreLabel.Location = new System.Drawing.Point(48, 20);
            this.ChooseStoreLabel.Name = "ChooseStoreLabel";
            this.ChooseStoreLabel.Size = new System.Drawing.Size(77, 13);
            this.ChooseStoreLabel.TabIndex = 0;
            this.ChooseStoreLabel.Text = "Choose Store: ";
            // 
            // CostOfOrdersLabel
            // 
            this.CostOfOrdersLabel.AutoSize = true;
            this.CostOfOrdersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CostOfOrdersLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.CostOfOrdersLabel.Location = new System.Drawing.Point(13, 269);
            this.CostOfOrdersLabel.Name = "CostOfOrdersLabel";
            this.CostOfOrdersLabel.Size = new System.Drawing.Size(280, 15);
            this.CostOfOrdersLabel.TabIndex = 9;
            this.CostOfOrdersLabel.Text = "Cost of Orders (based on selected inputs): ";
            // 
            // CostOfOrders
            // 
            this.CostOfOrders.AutoSize = true;
            this.CostOfOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CostOfOrders.ForeColor = System.Drawing.Color.Black;
            this.CostOfOrders.Location = new System.Drawing.Point(299, 268);
            this.CostOfOrders.Name = "CostOfOrders";
            this.CostOfOrders.Size = new System.Drawing.Size(0, 17);
            this.CostOfOrders.TabIndex = 10;
            // 
            // ReportOutputListbox
            // 
            this.ReportOutputListbox.FormattingEnabled = true;
            this.ReportOutputListbox.Location = new System.Drawing.Point(13, 323);
            this.ReportOutputListbox.Name = "ReportOutputListbox";
            this.ReportOutputListbox.Size = new System.Drawing.Size(886, 186);
            this.ReportOutputListbox.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(16, 304);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Report Generation History";
            // 
            // StartFilesProcessing
            // 
            this.StartFilesProcessing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartFilesProcessing.Location = new System.Drawing.Point(632, 88);
            this.StartFilesProcessing.Name = "StartFilesProcessing";
            this.StartFilesProcessing.Size = new System.Drawing.Size(256, 23);
            this.StartFilesProcessing.TabIndex = 13;
            this.StartFilesProcessing.Text = "Start Selected Directory File Processing";
            this.StartFilesProcessing.UseVisualStyleBackColor = true;
            this.StartFilesProcessing.Click += new System.EventHandler(this.StartFilesProcessing_Click);
            // 
            // FileProcessingDuration
            // 
            this.FileProcessingDuration.AutoSize = true;
            this.FileProcessingDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileProcessingDuration.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.FileProcessingDuration.Location = new System.Drawing.Point(614, 25);
            this.FileProcessingDuration.Name = "FileProcessingDuration";
            this.FileProcessingDuration.Size = new System.Drawing.Size(0, 15);
            this.FileProcessingDuration.TabIndex = 14;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 520);
            this.Controls.Add(this.FileProcessingDuration);
            this.Controls.Add(this.StartFilesProcessing);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ReportOutputListbox);
            this.Controls.Add(this.CostOfOrders);
            this.Controls.Add(this.CostOfOrdersLabel);
            this.Controls.Add(this.FinancialReportingInputsGroupbox);
            this.Controls.Add(this.LoadLookupData);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ChooseCSVFilesDirectoryGroupBox);
            this.MaximizeBox = false;
            this.Name = "Dashboard";
            this.ShowIcon = false;
            this.Text = "Reporting Tool for Spur Ordering System";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.ChooseCSVFilesDirectoryGroupBox.ResumeLayout(false);
            this.ChooseCSVFilesDirectoryGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressIndicator)).EndInit();
            this.FinancialReportingInputsGroupbox.ResumeLayout(false);
            this.FinancialReportingInputsGroupbox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.FolderBrowserDialog CSVFolderBrowserDialog;
        private System.Windows.Forms.TextBox CSVFilesSelectedDirectoryTextBox;
        private System.Windows.Forms.Button ChooseCSVFilesDirectoryButton;
        private System.Windows.Forms.GroupBox ChooseCSVFilesDirectoryGroupBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button LoadLookupData;
        private System.Windows.Forms.GroupBox FinancialReportingInputsGroupbox;
        private System.Windows.Forms.Label ChooseStoreLabel;
        private System.Windows.Forms.ComboBox ChooseStoresComboBox;
        private System.Windows.Forms.ComboBox ChooseSupplierTypeComboBox;
        private System.Windows.Forms.Label ChooseSupplierTypeLabel;
        private System.Windows.Forms.ComboBox ChooseSupplierComboBox;
        private System.Windows.Forms.Label ChooseSupplierLabel;
        private System.Windows.Forms.Label CostOfOrdersLabel;
        private System.Windows.Forms.Label CostOfOrders;
        private System.Windows.Forms.Button CalculateCost;
        private System.Windows.Forms.ComboBox ChooseYearCombobox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ChooseWeekNumberCombobox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox ReportOutputListbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox ProgressIndicator;
        private System.Windows.Forms.Button StartFilesProcessing;
        private System.Windows.Forms.Label FileProcessingDuration;
    }
}

